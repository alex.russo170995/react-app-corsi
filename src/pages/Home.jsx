import { useEffect } from "react";
import { Link } from "react-router-dom";
import { useContext } from "react";
import { AuthContext } from "../contexts/AuthContext";

export default function Home() {
  const { currentUser } = useContext(AuthContext);

  useEffect(() => {
    document.title = "Home";
  }, []);

  useEffect(() => {
  }, []);

  return(
    <div className=" my-5 d-flex flex-column align-items-center">
      <h1>Academy</h1>
      <p>Questo sito ti permette di seguire dei corsi.</p>
      {currentUser.nome == ""?<p><Link to="/login">Effettua l'accesso</Link> oppure <Link to="/registrazione">registrati</Link></p>:<></>}
    </div>
  )
}
